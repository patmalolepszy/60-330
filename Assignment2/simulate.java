/*
Austin G. Docherty
104058615
June 27, 2018
60-330 Assignment 2
*/
import java.util.*;

class ExponentialSampler {
    public ExponentialSampler(double lam) {
	double epsilon = 0.001;
	this.lambda = lam;
	this.upperBound = Math.log(lambda / epsilon) / lambda;
    }

    public double sample() {
	while (true) {
	    double x = Math.random() * upperBound;
	    double y = Math.random() * lambda;
	    if (y < lambda * Math.exp(-1 * lambda * x))
		return x;
	}
    }

    private double lambda;
    private double upperBound;
}

class Job {
    public Job(int pid, Random r) {
	this.id = pid;
	this.totalJobTime = r.nextGaussian() * 4.00 + 30.00; //Distribution*Standard Deviation + Mean
	    }

    public void runFor(double burstTime) {
	this.totalJobTime -= burstTime;
    }

    public void updateBurstEstimate(double burstTime) {
	this.burstEstimate = (0.5 * burstTime) + (1 - 0.5) * (burstEstimate); //Exponential Averaging with Alpha = 0.5
    }

    public int id;
    public double totalJobTime;
    public double burstEstimate = 1;
}

class MyComparator implements Comparator<Job> {
    public int compare(Job a, Job b) {
	if (a.burstEstimate > b.burstEstimate) return 1;
	if (a.burstEstimate < b.burstEstimate) return -1;
	else return 0;
    }
}
	
public class simulate{    
    public static void main(String [ ] args) {
	MyComparator comparator = new MyComparator();
	PriorityQueue<Job> jobQueue = new PriorityQueue<Job>(10, comparator);
	Random random = new Random();
    
	for(int i = 0; i < 10; i++) {
	    Job job = new Job(i, random);
	    System.out.println(job.id + " " + job.totalJobTime);
	    jobQueue.add(job);
	}
	
	System.out.println("***");
	
	ExponentialSampler expSampler = new ExponentialSampler(0.5);
	double burstTime;
	double totalTime = 0;
	PriorityQueue<Job> waitingJobs = new PriorityQueue<Job>(10, comparator);
	
	while(jobQueue.peek() != null || waitingJobs.peek() != null) {
	    Job nextJob = jobQueue.poll();
	    while(waitingJobs.peek() != null) {
		jobQueue.add(waitingJobs.poll());
	    }
	    if(nextJob != null) {
		burstTime = expSampler.sample();
		nextJob.runFor(burstTime);
		totalTime += burstTime;
		System.out.println(nextJob.id + " " + burstTime);
		if(nextJob.totalJobTime > 0.00) {
		    nextJob.updateBurstEstimate(burstTime);
		    waitingJobs.add(nextJob);
		}
	    }
	}
	System.out.println("***");
	System.out.println(totalTime);    
    }
}
